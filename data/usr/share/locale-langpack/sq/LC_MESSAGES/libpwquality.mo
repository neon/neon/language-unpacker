��          �      ,      �     �  
   �     �     �     �     �     �       *   ,  5   W  5   �  $   �     �  '     *   -     X  �  f     �  
                  3  &   S  )   z     �  *   �  7   �  7   %  &   ]  %   �  +   �  /   �                       
                       	                                     BAD PASSWORD: %s Error: %s
 Fatal failure Memory allocation error New %s%spassword:  No password supplied Retype new %s%spassword:  Sorry, passwords do not match. The password contains less than %ld digits The password contains less than %ld lowercase letters The password contains less than %ld uppercase letters The password contains too few digits The password is a palindrome The password is the same as the old one The password is too similar to the old one Unknown error Project-Id-Version: libpwquality 1.2.4
Report-Msgid-Bugs-To: http://fedorahosted.org/libpwquality
PO-Revision-Date: 2017-09-04 14:57+0000
Last-Translator: Sidorela Uku <Unknown>
Language-Team: Albanian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2018-07-12 14:48+0000
X-Generator: Launchpad (build 18719)
Language: sq
 FJALËKALIM I KEQ: %s Gabim: %s
 Dështim fatal Gabim sigurimi kujtese Fjalëkalim i ri %s%spassword:  Nuk është dhënë asnjë fjalëkalim Rishtyp fjalëkalimin e ri %s%spassword:  Fjalëkalimet nuk përputhen. Fjalëkalimi përmban më pak se %ld numra Fjalëkalimi përmban më pak se %ld shkronja të vogla Fjalëkalimi përbam më pak se %ld shkronja të mëdha Fjalëkalimi përmban shumë pak numra Fjalëkalimi është një palindromë Fjalëkalimi është i njëjtë si i vjetri Fjalëkalimi është i ngjashëm me të vjetrin Gabim i panjohur 