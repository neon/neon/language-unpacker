At the time of writing only used for https://bugs.launchpad.net/ubuntu/+source/libpwquality/+bug/1834480

THIS MAY NOT BE NECESSARY IN 20.04!!!!!

When moving to 20.04 make sure if we still need this package or kill
it. As a reminder of this the preinst will error out fatally when run
against a wrong version.

- debian/update.rb generates the data dir, which is simply an extracted
  dump of the pertinent files.

- debian/gen.rb further generates the maintainer scripts with a list
  of the extracted files
